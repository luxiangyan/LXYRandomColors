# LXYRandomColorsProectr

[![CI Status](https://img.shields.io/travis/lxy/LXYRandomColorsProectr.svg?style=flat)](https://travis-ci.org/lxy/LXYRandomColorsProectr)
[![Version](https://img.shields.io/cocoapods/v/LXYRandomColorsProectr.svg?style=flat)](https://cocoapods.org/pods/LXYRandomColorsProectr)
[![License](https://img.shields.io/cocoapods/l/LXYRandomColorsProectr.svg?style=flat)](https://cocoapods.org/pods/LXYRandomColorsProectr)
[![Platform](https://img.shields.io/cocoapods/p/LXYRandomColorsProectr.svg?style=flat)](https://cocoapods.org/pods/LXYRandomColorsProectr)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LXYRandomColorsProectr is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LXYRandomColorsProectr'
```

## Author

lxy, 1556937213@qq.com

## License

LXYRandomColorsProectr is available under the MIT license. See the LICENSE file for more info.
