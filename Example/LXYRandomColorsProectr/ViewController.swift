//
//  ViewController.swift
//  LXYRandomColorsProectr
//
//  Created by lxy on 11/30/2018.
//  Copyright (c) 2018 lxy. All rights reserved.
//

import UIKit
import LXYRandomColorsProectr

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let vc = RandColors()
        let randColor = vc.random()
        self.view.backgroundColor = randColor
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

