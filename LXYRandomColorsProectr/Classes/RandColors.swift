//
//  RandColors.swift
//  LXYRandomColorsProectr
//
//  Created by GY on 2018/12/3.
//

import Foundation
// 记住这里有个作用域
public class RandColors : UIViewController  {
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    public func random() -> UIColor {
        
        let red = CGFloat(arc4random()%256)/255.0
        let green = CGFloat(arc4random()%256)/255.0
        let blue = CGFloat(arc4random()%256)/255.0
        return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
        
    }
    
}

